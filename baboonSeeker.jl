function fail()
    i = 1
    while i > 0
        print("Failure")
        for j in 1:i
            print("!")
        end
        println()
        i += 1
    end
end

function stumble(inventory, bamboo)
    droppedType = rand(1:2)
    if bamboo == 0
        droppedType = 1
    end

    if droppedType == 1
        droppedItem = rand(1:length(inventory))
        println("You stumbled and dropped a ", inventory[droppedItem])
        if inventory[droppedItem] == "Baboon Seeking Backpack"
            fail()
        else
            deleteat!(inventory, droppedItem)
        end
        return inventory, bamboo
    else
        bamboos = rand(1:bamboo)
        println("You stumbled and dropped ", bamboos, " bamboo")
        bamboo -= bamboos
        return inventory, bamboo
    end
end

function showInv(inventory, bamboo)
    print("YOU HAS ", bamboo, " BAMBOO")
    for i in 1:length(inventory)
        print(" AND YOU HAS A ", inventory[i])
    end
    println()
end

function seek(inventory, bamboo)
    if bamboo > 0
        bamboo -= 1
    else
        println("YOU HAS NO BAMBOO!")

    seeked = rand(0:100)
    if seeked < 1
        println("You soke a Bamboon")
        inventory, bamboo = stumble(inventory, bamboo)
    elseif seeked < 10
        if bamboo > 0
            println("You soke a Baboon")
            push!(inventory, "Baboon")
        else
            println("You soke a Baboon but it ran away because you are out of bamboo");

    elseif seeked < 50
        println("You soke a Bamboo")
        bamboo += 2
    else
        println("You soke a nothing")
    end
    return inventory, bamboo
end

function main()
    println("WELCOME TO BABOON SEEKER XTREME JULIA")
    bamboo = rand(5:20)
    inventory = ["Baboon Seeking Backpack"]
    println("You are the mighty baboon seeker. You have set off into the coral reef with your baboon seeking backpack holding ", bamboo, " bamboo to use for seeking baboons.")
    while true
        print(": ")
        cmd = readline()
        if cmd == "seek"
            inventory, bamboo = seek(inventory, bamboo)
        elseif cmd == "WHAT DOES I HAS"
            showInv(inventory, bamboo)
        else
            inventory, bamboo = stumble(inventory, bamboo)
        end
    end

end

main()