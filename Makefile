all: require c cpp cs cob go goPerformance hx java kt rust typescript

clean:
	rm -rf bin

bindir:
	mkdir -p bin

require:
	@echo "Checking the programs required for building..."
	@cc --version >/dev/null 2>&1 || (echo "ERROR: cc is required."; exit 1)
	@cobc --version >/dev/null 2>&1 || (echo "ERROR: cobc is required."; exit 1)
	@c++ --version >/dev/null 2>&1 || (echo "ERROR: c++ is required."; exit 1)
	@mcs --version >/dev/null 2>&1 || (echo "ERROR: mcs is required."; exit 1)
	@go version >/dev/null 2>&1 || (echo "ERROR: go is required."; exit 1)
	@haxe --version >/dev/null 2>&1 || (echo "ERROR: haxe is required."; exit 1)
	@javac -version >/dev/null 2>&1 || (echo "ERROR: javac is required."; exit 1)
	@kotlinc -version >/dev/null 2>&1 || (echo "ERROR: kotlinc is required."; exit 1)
	@cargo -version >/dev/null 2>&1 || (echo "ERROR: cargo is required."; exit 1)
	@npm --version >/dev/null 2>&1 || (echo "ERROR: npm is required."; exit 1)
	@tsc --version >/dev/null 2>&1 || (echo "ERROR: tsc is required."; exit 1)

c: baboonSeeker.c bindir
	cc baboonSeeker.c -o bin/baboonSeekerC
cob: baboonSeeker.cob bindir
	cobc -x -free baboonSeeker.cob -o bin/baboonSeekerCobol
cpp: baboonSeeker.cpp bindir
	c++ baboonSeeker.cpp -o bin/baboonSeekerCpp
cs: baboonSeeker.cs bindir
	mcs -out:bin/baboonSeekerCs baboonSeeker.cs
go: baboonSeeker.go bindir
	go build -o bin/baboonSeekerGo baboonSeeker.go
goPerformance: baboonSeekerPerformance.go bindir
	go build -o bin/baboonSeekerPerformanceGo baboonSeekerPerformance.go
hx: BaboonSeeker.hx
	mkdir -p bin/haxe
	mkdir -p bin/haxe/{php,cpp,cs,java}

	haxe BaboonSeeker.hx -lib random --lua bin/haxe/haxe.lua --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --neko bin/haxe/haxe.neko --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --php bin/haxe/php --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --cpp bin/haxe/cpp --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --cppia bin/haxe/haxe.cppia --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --cs bin/haxe/cs --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --java bin/haxe/java --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --jvm bin/haxe/haxe.class --main BaboonSeeker
	haxe BaboonSeeker.hx -lib random --python bin/haxe/haxe.py --main BaboonSeeker
java: baboonSeeker.java bindir
	javac baboonSeeker.java -d bin
kt: baboonSeeker.kt bindir
	kotlinc baboonSeeker.kt -include-runtime -d bin/baboonSeekerKotlin.jar
rust: baboonSeekerRust/src/main.rs bindir
	cargo build --manifest-path baboonSeekerRust/Cargo.toml --release
	mv baboonSeekerRust/target/release/baboon_seeker bin/baboonSeekerRust
typescript: baboonSeeker.ts bindir
	npm i
	tsc --outFile bin/baboonSeekerTypescript.js baboonSeeker.ts
