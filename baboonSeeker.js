const readline = require('readline');

function randomIntFromInterval(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

function fail(){
	for(var i = 1; i > 0; i++){
		process.stdout.write("Failure");
		for(var j = 0; j < i; j++){
			process.stdout.write("!");
		}
		console.log();
	}
}

function seek(inventory, bamboo){
	if (bamboo > 0) {
		bamboo--;
	} else {
		console.log("YOU HAS NO BAMBOO!");
	}

	var seeked = randomIntFromInterval(0, 100);
	if (seeked < 1) {
		console.log("You soke a Bamboon");
		inventory, bamboo = stumble(inventory, bamboo);
	} else if (seeked < 10) {
		if (bamboo > 0) {
			console.log("You soke a Baboon");
			inventory.push("Baboon");
		} else {
			console.log("You soke a Baboon but it ran away because you are out of bamboo");
		}
	} else if (seeked < 50) {
		console.log("You soke a Bamboo");
		bamboo+=2;
	} else {
		console.log("You soke a nothing");
	}
	return inventory, bamboo;
}

function showInv(inventory, bamboo){
	process.stdout.write("YOU HAS " + bamboo + " BAMBOO");
	for (var i = 0; i < inventory.length; i++){
		process.stdout.write(" AND YOU HAS A " + inventory[i]);
	}
	console.log();
}

function stumble(inventory, bamboo){
	var droppedType = randomIntFromInterval(1, 2);
	if (bamboo == 0){
		droppedType = 1;
	}
	if (droppedType == 1){
		var droppedItem = randomIntFromInterval(0, inventory.length - 1);
		var droppedItemName = inventory[droppedItem];
		inventory.splice(droppedItem, 1);
		console.log("You stumbled and dropped a " + droppedItemName);
		console.log(droppedItemName)
		if(droppedItemName == "Baboon Seeking Backpack"){
			fail();
		}
		return inventory, bamboo;
	}else{
		var bamboos = randomIntFromInterval(1, bamboo);
		console.log("You stumbled and dropped " + bamboos + " bamboo");
		bamboo -= bamboos;
		return inventory, bamboo;
	}
}

async function* promptUser(query) {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	});

	try {
		for (;;) {
			yield new Promise((resolve) => rl.question(query, resolve));
		}
	} finally {
		rl.close();
	}
}

async function main(){
	console.log("WELCOME TO BABOON SEEKER XTREME JS");
	var bamboo = randomIntFromInterval(5, 20);
	var inventory = ["Baboon Seeking Backpack"];
	console.log("You are the mighty baboon seeker. You have set off into the ocean with your baboon seeking backpack holding " + bamboo + " bamboo to use for seeking baboons.");
	while (true){
		for await (const cmd of promptUser(": ")) {
			if (cmd == "seek"){
				inventory, bamboo = seek(inventory, bamboo);
			}else if(cmd == "WHAT DOES I HAS"){
				showInv(inventory, bamboo);
			}else{
				inventory, bamboo = stumble(inventory, bamboo);
			}
		}
	}
}
main();