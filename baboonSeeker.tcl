#!/usr/bin/tclsh

proc randInt {min max} {
	return [expr {int(rand() * ($max + 1 - $min)) + $min}]
}

proc fail {} {
	for {set i 1} {$i > 0} {incr i} {
		puts -nonewline "Failure"
		flush stdout
		for {set j 0} {$j < $i} {incr j} {
			puts -nonewline "!"
			flush stdout
		}
		puts ""
	}
}

proc stumble {inventory bamboo} {
	set droppedType [randInt 1 2]
	if {$bamboo == 0} {
		set droppedType 1
	}

	if {$droppedType == 1} {
		set droppedItem [randInt 0 [expr [llength $inventory] - 1]]
		set droppedItemName [lindex $inventory $droppedItem]
		set inventory [lreplace $inventory $droppedItem $droppedItem]
		puts "You stumbled and dropped a $droppedItemName"

		if {$droppedItemName == "Baboon Seeking Backpack"} {
			fail
		}

	} else {
		set bamboos [randInt 1 $bamboo]
		puts "You stumbled and dropped $bamboos bamboo"
		incr bamboo [expr -1 * $bamboos]
	}

	return [list $inventory $bamboo]
}

proc showInv {inventory bamboo} {
	puts -nonewline "YOU HAS $bamboo BAMBOO"
	foreach item $inventory {
		puts -nonewline " AND YOU HAS A $item"
	}

	flush stdout
	puts ""
}

proc seekItem {inventory bamboo} {
	if {$bamboo > 0} {
		incr bamboo -1
	} else {
		puts "YOU HAS NO BAMBOO!"
	}

	set seeked [randInt 0 100]
	if {$seeked < 1} {
		puts "You soke a Bamboon"
		set seekResult [stumble $inventory $bamboo]
		set inventory [lindex $seekResult 0]
		set bamboo [lindex $seekResult 1]
	} elseif {$seeked < 10} {
		if {$bamboo > 0} {
			puts "You soke a Baboon"
			lappend inventory "Baboon"
		} else {
			puts "You soke a Baboon but it ran away because you are out of bamboo"
		}
		
	} elseif {$seeked < 50} {
		puts "You soke a Bamboo"
		incr bamboo 2
	} else {
		puts "You soke a nothing"
	}

	return [list $inventory $bamboo]
}

proc main {} {
	puts "WELCOME TO BABOON SEEKER XTREME TCL"
	set bamboo [randInt 5 20]
	set inventory {"Baboon Seeking Backpack"}
	puts "You are the mighty baboon seeker. You have set off into the cavern with your baboon seeking backpack holding $bamboo bamboo to use for seeking baboons."

	while {1} {
		puts -nonewline ": "
		flush stdout

		gets stdin cmd

		if {$cmd == "seek"} {
			set seekResult [seekItem $inventory $bamboo]
			set inventory [lindex $seekResult 0]
			set bamboo [lindex $seekResult 1]

		} elseif {$cmd == "WHAT DOES I HAS"} {
			showInv $inventory $bamboo
		} else {
			set seekResult [stumble $inventory $bamboo]
			set inventory [lindex $seekResult 0]
			set bamboo [lindex $seekResult 1]
		}
	}
}

main