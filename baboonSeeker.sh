#!/usr/bin/env bash

fail(){
	for ((i = 1; i > 0; i++)); do
		printf "Failure"
		for ((j = 0; j < i; j++)); do
			printf "!"
		done
		echo
	done
}

seek(){
	if [[ "$bamboo" -gt "0" ]]; then
		bamboo=$((bamboo - 1))
	else
		echo "YOU HAS NO BAMBOO!"
	fi

	seeked=$(shuf -i 0-100 -n 1)
	if [[ "$seeked" -le "1" ]]; then
		echo "You soke a Bamboon"
		stumble $inventory $bamboo
	elif [[ "$seeked" -le "10" ]]; then
		if [[ "$bamboo" -gt "0" ]]; then
			echo "You soke a Baboon"
			inventory+=("Baboon")
		else
			echo "You soke a Baboon but it ran away because you are out of bamboo"
		fi
	elif [[ "$seeked" -le "50" ]]; then
		echo "You soke a Bamboo"
		bamboo=$((bamboo + 2))
	else
		echo "You soke a nothing"
	fi
}

showInv(){
	printf "YOU HAS ${bamboo} BAMBOO"
	for ((i = 0; i < ${#inventory[@]}; i++)); do
		printf " AND YOU HAS A ${inventory[$i]}"
	done
	echo
}

stumble(){
	droppedType=$(shuf -i 1-2 -n 1)
	if [[ "$bamboo" == "0" ]]; then
		droppedType=1
	fi

	if [[ "$droppedType" == "1" ]]; then
		arrayLength=${#inventory[@]}
		arrayLength=$((arrayLength - 1))
		randomIndex=$(shuf -i 0-$arrayLength -n 1)
		droppedItem="${inventory[randomIndex]}"

		# Build new array
		new_array=()
		for i in "${!inventory[@]}"; do
			if [[ "$i" != "$randomIndex" ]]; then
				new_array+=("${inventory[i]}")
			fi
		done
		inventory=("${new_array[@]}")
		unset new_array

		echo "You stumbled and dropped a ${droppedItem}"
		if [[ "$droppedItem" == "Baboon Seeking Backpack" ]]; then
			fail
		fi
	else
		bamboos=$(shuf -i 1-$bamboo -n 1)
		echo "You stumbled and dropped ${bamboos} bamboo"
		bamboo=$((bamboo - bamboos))
	fi
}

main(){
	echo "WELCOME TO BABOON SEEKER XTREME SHELL"
	bamboo=$(shuf -i 5-20 -n 1)
	inventory=("Baboon Seeking Backpack")
	echo "You are the mighty baboon seeker. You have set off into the badlands with your baboon seeking backpack holding ${bamboo} bamboo to use for seeking baboons."

	while true; do
	read -p ": " cmd

		if [[ "$cmd" == "seek" ]]; then
			seek $inventory $bamboo
		elif [[ "$cmd" == "WHAT DOES I HAS" ]]; then
				showInv $inventory $bamboo
		else
			stumble $inventory $bamboo
		fi
	done
}

main
