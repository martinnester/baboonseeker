#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define ARRAY_SIZE 100
#define MAX_INVENTORY_ITEM_SIZE 30

/////////////////////
// Utility Functions
/////////////////////
unsigned long mix(unsigned long a, unsigned long b, unsigned long c) {
	a=a-b;  a=a-c;  a=a^(c >> 13);
	b=b-c;  b=b-a;  b=b^(a << 8);
	c=c-a;  c=c-b;  c=c^(b >> 13);
	a=a-b;  a=a-c;  a=a^(c >> 12);
	b=b-c;  b=b-a;  b=b^(a << 16);
	c=c-a;  c=c-b;  c=c^(b >> 5);
	a=a-b;  a=a-c;  a=a^(c >> 3);
	b=b-c;  b=b-a;  b=b^(a << 10);
	c=c-a;  c=c-b;  c=c^(b >> 15);
	return c;
}

int randint(int min, int max) {
	// inclusive random number generator
	return (rand() % (max - min + 1)) + min;
}

void append_item(char** inventory, char* to_append, size_t* inventory_next_index, size_t* inventory_buffer_size) {
	if (*inventory_next_index > *inventory_buffer_size) {
		// double array memory allocation
		(*inventory_buffer_size) *= 2;
		char* tmp = realloc(inventory, (*inventory_buffer_size) * sizeof(char*));
		*inventory = tmp;
		// dont free temp as that creates a SEGV after 1 realloc
		// im not entirely sure what the correct way to do this is, but this method
		// seems to work up to a buffer of 25600
	}

	// set current index to the pointer which we passed as `to_append`
	inventory[(*inventory_next_index)++] = to_append;
}

void remove_item(char** inventory, int index_to_remove, size_t* inventory_next_index) {
	// move everything ahead of the selected element back one index
	for (int i = index_to_remove + 1; i < *inventory_next_index; i++) {
		inventory[i - 1] = inventory[i];
	}

	// set excess element to null
	inventory[--(*inventory_next_index)] = '\0';
}

/////////////////////
// Main program
/////////////////////

void fail() {
	for (int i = 1; i > 0; i++) {
		printf("Failure");
		for (int j = 0; j < i; j++) {
			printf("!");
		}
		printf("\n");
	}
}

void stumble(char** inventory, size_t* inventory_next_index, int* bamboo) {
	int droppedType = randint(1, 2);
	if (*bamboo == 0) {
		droppedType = 1;
	}

	if (droppedType == 1) {
		int droppedItemIndex = randint(0, (*inventory_next_index) - 1);
		char* droppedItemName = inventory[droppedItemIndex];
		printf("You stumbled and dropped a %s\n", droppedItemName);
		remove_item(inventory, droppedItemIndex, inventory_next_index);

		if (strcmp(droppedItemName, "Baboon Seeking Backpack") == 0) {
			fail();
		}

	} else {
		int bamboos = randint(1, *bamboo);
		printf("You stumbled and dropped %d bamboo\n", bamboos);
		(*bamboo) -= bamboos;
	}
}

void showInv(char** inventory, size_t* inventory_next_index, int* bamboo) {
	printf("YOU HAS %d BAMBOO", *bamboo);
	for (int i = 0; i < *inventory_next_index; i++) {
		printf(" AND YOU HAS A %s", inventory[i]);
	}
	printf("\n");
}

void seek(char** inventory, size_t* inventory_next_index, int* bamboo, size_t* inventory_buffer_size) {
	if (*bamboo > 0) {
		(*bamboo)--;
	} else {
		printf("YOU HAS NO BAMBOO!\n");
	}
	
	int seeked = randint(0, 100);
	if (seeked < 1) {
		printf("You soke a Bamboon\n");
		stumble(inventory, inventory_next_index, bamboo);
	} else if (seeked < 10) {
		if (*bamboo > 0) {
			printf("You soke a Baboon\n");
			append_item(inventory, "Baboon", inventory_next_index, inventory_buffer_size);
		} else {
			printf("You soke a Baboon but it ran away because you are out of bamboo\n");
		}
	} else if (seeked < 50) {
		printf("You soke a Bamboo\n");
		(*bamboo) += 2;
	} else {
		printf("You soke a nothing\n");
	}
}

int main() {
	unsigned long seed = mix(clock(), time(NULL), getpid());
	srand(seed);

	printf("WELCOME TO BABOON SEEKER XTREME C\n");
	int bamboo = randint(5, 20);

	// initialize inventory
	size_t inventory_buffer_size = ARRAY_SIZE;
	char** inventory = (char**) malloc(inventory_buffer_size * sizeof(char*));
	size_t inventory_next_index = 0;
	append_item(inventory, "Baboon Seeking Backpack", &inventory_next_index, &inventory_buffer_size);

	printf("You are the mighty baboon seeker. You have set off into the suburbs with your baboon seeking backpack holding %d bamboo to use for seeking baboons.\n", bamboo);

	for (;;) {
		// take user input
		char input[MAX_INVENTORY_ITEM_SIZE];
		printf(": ");
		fgets(input, MAX_INVENTORY_ITEM_SIZE, stdin);

		if (strcmp(input, "seek\n") == 0) {
			seek(inventory, &inventory_next_index, &bamboo, &inventory_buffer_size);
		} else if (strcmp(input, "WHAT DOES I HAS\n") == 0) {
			showInv(inventory, &inventory_next_index, &bamboo);
		} else {
			stumble(inventory, &inventory_next_index, &bamboo);
		}
	}

	return 0;

}
