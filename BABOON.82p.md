## TI-82 Baboon Seeker Naming Documentation
The TI-82 version of TI-Basic only allows single characters for variable names and numbers for label names, this document explains these because it doesn't allow comments and otherwise it would be incomprehensible

### Labels:
- 0 - Command input
- 1 - Seek
- 2 - Inventory
- 3 - Stumble

### Variables:
- A - Baboon seeking backpack
- B - Bamboo
- C - Baboons
- J - Selecctor
