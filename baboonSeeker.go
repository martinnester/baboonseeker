package main
import (
	"fmt"
	"bufio"
	"os"
	"math/rand"
	"strings"
	"time"
)
var inventory = []string{"Baboon Seeking Backpack"}
var bamboo = 0
func remove(s []string, i int) []string {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
func input(prompt string) string {
	reader := bufio.NewReader(os.Stdin)
	var receivedInput string
	fmt.Print(prompt)
	receivedInput, _ = reader.ReadString('\n')
	receivedInput = strings.Trim(receivedInput, "\n")
	return receivedInput
}
func give(item string){
	inventory = append(inventory, item)
}
func seek(){
	seeked := rand.Intn(99)
	if seeked == 0{
		println("You soke a Bamboon")
		stumble()
	} else if seeked <= 10{
		if bamboo > 0 {
			println("You soke a Baboon")
			give("Baboon")
		} else {
			println("You soke a baboon but it ran away because you are out of bamboo")
		}
	} else if seeked <= 60 {
		println("You soke a bamboo")
		bamboo++
		bamboo++

	} else{
		println("You soke a nothing")
	}
	if bamboo == 0{
		println("YOU HAS NO BAMBOO!")
	}else{
		bamboo--
	}
}
func stumble(){
	rand.Seed(time.Now().UTC().UnixNano())
	droppedType := rand.Intn(2)
	if droppedType == 0 {
		rand.Seed(time.Now().UTC().UnixNano()+int64(rand.Intn(999999))/int64(rand.Intn(999999))*int64(rand.Intn(999999)))
		dropped := rand.Intn(len(inventory))
		println("You stumbled and dropped a", (inventory)[dropped])
		if inventory[dropped] == "Baboon Seeking Backpack"{
			println("You cannot seek Baboons without a Baboon Seeking Backpack!")
			os.Exit(0)
		}
		inventory = remove(inventory, dropped)
	} else if droppedType == 1 {
		var bamboos=rand.Intn(len(inventory))+1
		println("You stumbled and dropped ", bamboos, " bamboo")
		bamboo-=bamboos
	}

}
func showInv(){
	print("YOU HAS ",bamboo," BAMBOO")
	for _, item := range (inventory){
		print(" AND YOU HAS A ",item)
	}
}
func parse(cmd string){
	if cmd == "seek"{
		seek()
	} else if cmd == "WHAT DOES I HAS"{
		showInv()
	} else{
		stumble()
	}
}
func main(){
	print("\033[H\033[2J")
	rand.Seed(time.Now().UTC().UnixNano())
	bamboo = rand.Intn(20-5) + 5
	println("Welcome to Baboon Seeker Xtreme Go Edition")
	println("You are a mighty baboon seeker who has set off into the desert with", bamboo, "bamboo to use for seeking baboons.")
	for true{
		cmd := input("\n: ")
		parse(cmd)
	}
}