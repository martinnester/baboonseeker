using System;
using System.Collections.Generic;

namespace BaboonSeekerGame {
	class BaboonSeeker {
		static void Main(string[] args){
			Console.WriteLine("WELCOME TO BABOON SEEKER XTREME C#");
			Random random = new Random();
			int bamboo = random.Next(5, 21);
			List<string> inventory = new List<string>();
			inventory.Add("Baboon Seeking Backpack");
			Console.WriteLine("You are the mighty baboon seeker. You have set off into the taiga with your baboon seeking backpack holding " + bamboo + " bamboo to use for seeking baboons.");
			while (true) {
				Console.Write(": ");
				string cmd = Console.ReadLine();
				if (cmd == "seek"){
					var res = Seek(inventory, bamboo);
					inventory = res.inventory;
					bamboo = res.bamboo;
				}else if(cmd == "WHAT DOES I HAS"){
					ShowInv(inventory, bamboo);
				} else {
					var res = Stumble(inventory, bamboo);
					inventory = res.inventory;
					bamboo = res.bamboo;
				}
			}
		}

		static (List<string> inventory, int bamboo) Seek(List<string> inventory, int bamboo){
			Random random = new Random();
			int seeked = random.Next(0, 101);

			if (bamboo > 0) {
				bamboo--;
			} else {
				Console.WriteLine("YOU HAS NO BAMBOO!");
			}

			if (seeked < 1){
				Console.WriteLine("You soke a Bamboon");
				var res = Stumble(inventory, bamboo);
				inventory = res.inventory;
				bamboo = res.bamboo;
			}else if (seeked < 10){
				if (bamboo > 0) {
					Console.WriteLine("You soke a Baboon");
					inventory.Add("Baboon");
				} else {
					Console.WriteLine("You soke a Baboon but it ran away because you are out of bamboo");
				}
			}else if (seeked < 50){
				Console.WriteLine("You soke a Bamboo");
				bamboo += 2;
			} else {
				Console.WriteLine("You soke a nothing");
			}
			return (inventory, bamboo);
		}

		static (List<string> inventory, int bamboo) Stumble(List<string> inventory, int bamboo){
			Random random = new Random();
			int droppedType = random.Next(1, 3);
			if (bamboo == 0){
				droppedType = 1;
			}

			if (droppedType == 1){
				int droppedItem = random.Next(0, inventory.Count);
				string droppedItemName = inventory[droppedItem];
				inventory.RemoveAt(droppedItem);
				Console.WriteLine("You stumbled and dropped a " + droppedItemName);
				if (droppedItemName == "Baboon Seeking Backpack"){
					Fail();
				}
				return (inventory, bamboo);
			} else {
				int bamboos = random.Next(1, bamboo + 1);
				Console.WriteLine("You stumbled and dropped " + bamboos + " bamboo");
				bamboo--;
				return (inventory, bamboo);
			}
		}

		static void ShowInv(List<string> inventory, int bamboo){
			Console.Write("YOU HAS " + bamboo + " BAMBOO");
			foreach (string item in inventory){
				Console.Write(" AND YOU HAS A " + item);
			}
			Console.WriteLine();
		}

		static void Fail(){
			for(int i = 1; i > 0; i++){
				Console.Write("Failure");
				for(int j = 0; j < i; j++){
					Console.Write("!");
				}
				Console.WriteLine();
			}
		}
	}
}