function Fail{
	for ($i = 1; $i -gt 0; $i++){
		Write-Host -NoNewline "Failure"
		for ($j = 0; $j -lt $i; $j++){
			Write-Host -NoNewline "!"
		}
		Write-Host ""
	}
}

function Seek($inventory, $bamboo){
	if ($bamboo -gt 0) {
		$bamboo -= 1
	} else {
		Write-Host "YOU HAS NO BAMBOO!"
	}

	$seeked = Get-Random -Minimum 0 -Maximum 100
	if ($seeked -lt 1){
		Write-Host "You soke a Bamboon"
		$inventory, $bamboo = Stumble $inventory $bamboo
	} elseif ($seeked -lt 10){
		if ($bamboo -gt 0) {
			Write-Host "You soke a Baboon"
			$inventory = $inventory += "Baboon"
		} else {
			Write-Host "You soke a Baboon but it ran away because you are out of bamboo"
		}
	} elseif ($seeked -lt 50){
		Write-Host "You soke a Bamboo"
		$bamboo += 2
	} else {
		Write-Host "You soke a nothing"
	}
	[pscustomobject]@{
		inventory = $inventory
		bamboo = $bamboo
	}
}

function Show-Inv($inventory, $bamboo){
	Write-Host -NoNewline "YOU HAS $bamboo BAMBOO"
	foreach ($item in $inventory){
		Write-Host -NoNewline " AND YOU HAS A $item"
	}
	Write-Host ""
}

function Stumble($inventory, $bamboo){
	$droppedType = Get-Random -Minimum 1 -Maximum 2
	if ($bamboo -eq 0){
		$droppedType = 1
	}

	if ($droppedType -eq 1){
		if ($inventory.Count -eq 1){
			$randomIndex = 0
		} else {
			$randomIndex = Get-Random -Minimum 0 -Maximum ($inventory.Count - 1)
		}
		$droppedItemName = $inventory[$randomIndex]

		# Build new array
		$newInventory = @()
		for($i = 0; $i -le $inventory.Count; $i++){
			if($i -ne $randomIndex -and $i -ne $inventory.Count){
				$newInventory = $newInventory += $inventory[$i]
			}
		}
		$inventory = $newInventory

		Write-Host "You stumbled and dropped a $droppedItemName"
		if ($droppedItemName -eq "Baboon Seeking Backpack"){
			Fail
		}
		[pscustomobject]@{
			inventory = $inventory
			bamboo = $bamboo
		}
	} else {
		$bamboos = Get-Random -Minimum 1 -Maximum $bamboo
		Write-Host "You stumbled and dropped $bamboos bamboo"
		$bamboo -= $bamboos
		[pscustomobject]@{
			inventory = $inventory
			bamboo = $bamboo
		}
	}
}

function Main{
	Write-Host "WELCOME TO BABOON SEEKER XTREME POWERSHELL"
	$bamboo = Get-Random -Minimum 5 -Maximum 20
	$inventory = @("Baboon Seeking Backpack")
	Write-Host "You are the mighty baboon seeker. You have set off into the temperate forest with your baboon seeking backpack holding $bamboo bamboo to use for seeking baboons."

	while ($true){
		$cmd = Read-Host ": "
		if ($cmd -eq "seek"){
			$resp = Seek $inventory $bamboo
			$inventory = $resp.inventory
			$bamboo = $resp.bamboo
			} elseif ($cmd -eq "WHAT DOES I HAS"){
				Show-Inv $inventory $bamboo
			}else{
				$resp = Stumble $inventory $bamboo
				$inventory = $resp.inventory
				$bamboo = $resp.bamboo
			}
		}
	}

	Main