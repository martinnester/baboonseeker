use rand::{prelude::IteratorRandom, Rng};
use std::io::{self, Write};

fn seek(inventory: &mut Vec<&str>, bamboo: &mut i32) {
    if *bamboo > 0 {
        *bamboo -= 1;
    } else {
        println!("YOU HAS NO BAMBOO!");
    }

    let seeked = rand::thread_rng().gen_range(0..=100);
    if seeked < 1 {
        println!("You soke a Bamboon");
        stumble(inventory, bamboo);
    } else if seeked < 10 {
        if *bamboo > 0 {
            println!("You soke a Baboon");
            inventory.push("Baboon");
        } else {
            println!("You soke a Baboon but it ran away because you are out of bamboo");
        }
    } else if seeked < 50 {
        println!("You soke a Bamboo");
        *bamboo += 2;
    } else {
        println!("You soke a nothing");
    }
}

fn remove_random(inventory: &mut Vec<&str>) -> String {
    let (i, &out) = inventory
        .iter()
        .enumerate()
        .choose(&mut rand::thread_rng())
        .unwrap();
    inventory.remove(i);
    String::from(out)
}

fn show_inv(inventory: &mut Vec<&str>, bamboo: &mut i32) {
    print!("YOU HAS {} BAMBOO", bamboo);
    for item in inventory {
        print!(" AND YOU HAS A {}", item);
    }
    println!();
}

fn stumble(inventory: &mut Vec<&str>, bamboo: &mut i32) {
    let mut rng = rand::thread_rng();
    let dropped_type = rng.gen_range(0..=1);
    if (*bamboo == 0) || (dropped_type == 1) {
        let dropped_item = remove_random(inventory);
        println!("You stumbled and dropped a {:?}", dropped_item);
        if dropped_item == "Baboon Seeking Backpack" {
            fail();
        }
    } else {
        let bamboos = rng.gen_range(1..=*bamboo);
        println!("You stumbled and dropped {} bamboo", bamboos);
        *bamboo -= bamboos;
    }
}

fn fail() {
    let mut i = 1;
    while i > 0 {
        print!("Failure");
        for _ in 0..i {
            print!("!");
        }
        println!();
        i += 1;
    }
}

fn main() {
    println!("WELCOME TO BABOON SEEKER XTREME RUST");
    let mut bamboo = rand::thread_rng().gen_range(5..=20);
    let mut inventory = vec!["Baboon Seeking Backpack"];
    println!("You are the mighty baboon seeker. You have set off into the estuaries with you baboon seeking backpack holding {} bamboo to use for seeking baboons.", bamboo);

    loop {
        print!(": ");
        io::stdout().flush().unwrap();
        let mut cmd = String::new();
        match io::stdin().read_line(&mut cmd) {
            Ok(_) => (),
            Err(e) => panic!("Error when handling stdin: {:?}", e),
        };

        if cmd == "seek\n" {
            seek(&mut inventory, &mut bamboo);
        } else if cmd == "WHAT DOES I HAS\n" {
            show_inv(&mut inventory, &mut bamboo);
        } else {
            stumble(&mut inventory, &mut bamboo);
        }
    }
}
