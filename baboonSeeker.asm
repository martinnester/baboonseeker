section .bss
	user_input_buffer resb 100 ; user input buffer

section .data
	; printing text
	welcome_msg db "WELCOME TO BABOON SEEKER XTERME ASSEMBLY", 0xa
	welcome_msg_len equ $ - welcome_msg

	instructions_msg_part1 db "You are the mighty baboon seeker. You have set off into the fiery depths with your baboon seeking backpack holding "
	instructions_msg_part1_len equ $ - instructions_msg_part1

	instructions_msg_part2 db " bamboo to use for seeking baboons.", 0xa
	instructions_msg_part2_len equ $ - instructions_msg_part2

	failure_msg db "Failure"
	failure_msg_len equ $ - failure_msg

	input_prompt db ": "
	input_prompt_len equ $ - input_prompt

	seek_bamboon_message db "You soke a Bamboon", 0xa
	seek_bamboon_message_len equ $ - seek_bamboon_message

	seek_baboon_message db "You soke a Baboon", 0xa
	seek_baboon_message_len equ $ - seek_baboon_message

	seek_bamboo_message db "You soke a Bamboo", 0xa
	seek_bamboo_message_len equ $ - seek_bamboo_message

	seek_nothing_message db "You soke a nothing", 0xa
	seek_nothing_message_len equ $ - seek_nothing_message

	; seek messages
	seek_no_bamboo_msg db "YOU HAS NO BAMBOO!", 0xa
	seek_no_bamboo_msg_len equ $ - seek_no_bamboo_msg

	seek_baboon_fail_msg db "You soke a Baboon but it ran away because you are out of bamboo", 0xa
	seek_baboon_fail_msg_len equ $ - seek_baboon_fail_msg

	; inventory messages
	bamboo_first_half_msg db "YOU HAS "
	bamboo_first_half_len equ $ - bamboo_first_half_msg

	bamboo_second_half_msg db " BAMBOO"
	bamboo_second_half_len equ $ - bamboo_second_half_msg

	inventory_backpack_msg db " AND YOU HAS A Baboon Seeking Backpack"
	inventory_backpack_len equ $ - inventory_backpack_msg

	inventory_baboon_msg db " AND YOU HAS A Baboon"
	inventory_baboon_len equ $ - inventory_baboon_msg

	; stumble messages
	stumble_bamboo_first_msg db "You stumbled and dropped "
	stumble_bamboo_first_len equ $ - stumble_bamboo_first_msg

	stumble_bamboo_second_msg db " bamboo", 0xa
	stumble_bamboo_second_len equ $ - stumble_bamboo_second_msg

	stumble_item_backpack db "You stumbled and dropped a Baboon Seeking Backpack", 0xa
	stumble_item_backpack_len equ $ - stumble_item_backpack

	stumble_item_baboon db "You stumbled and dropped a Baboon", 0xa
	stumble_item_baboon_len equ $ - stumble_item_baboon

	; "seek\n"
	seek_string db "seek", 0xa, 0
	; "WHAT DOES I HAS\n"
	inventory_string db "WHAT DOES I HAS", 0xa, 0

	; game variables
	bamboo dw 0
	baboons dw 0

	; user input buffer length
	user_input_buffer_len equ 100

	; system variables
	str_buffer db 0
	seed dd 0

section .text
	global _start


;;;;;;;;;;;;;;;;;;;;;;;
;; String Comparison ;;
;;;;;;;;;;;;;;;;;;;;;;;
compare_strings:
	; move addresses of strings into rsi and rdi
	; returns in rax
	; 1 if not same, 0 if same
	mov al, byte [rsi]
	mov bl, byte [rdi]

	cmp al, bl
	je continue_comparison

	cmp al, 0 ; at the end of al
	je end_comparison
	cmp bl, 0 ; at the end of bl
	je end_comparison

	; not the same
	mov rax, 1
	ret

	continue_comparison:
		inc rsi
		inc rdi
		jmp compare_strings

	end_comparison:
		; strings are the same
		mov rax, 0
		ret


;;;;;;;;;;;;;;;;
;; User Input ;;
;;;;;;;;;;;;;;;;
take_input:
	; write input prompt
	mov rdx, input_prompt_len ; length
	mov rsi, input_prompt ; buffer
	mov rax, 1 ; write
	mov rdi, 1 ; stdout
	syscall

	; clear user input buffer
	xor rax, rax                    ; Set AL register to 0
	mov rdi, user_input_buffer      ; Load user_input address into RDI
	mov rcx, user_input_buffer_len  ; Load buffer length into RCX
	rep stosb                       ; Repeat store 0 in memory (clear buffer)

	; read user input
	mov rax, 0 ; read
	mov rdi, 0 ; stdin
	lea rsi, user_input_buffer ; buf
	mov rdx, user_input_buffer_len ; buflen
	syscall

	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rand() and srand() implementation ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
srand:
	; Get the current time in ms to use as the seed
	mov rax, 96 ; gettimeofday
	lea rdi, [rsp - 16] ; load address of time struct into rdi
	xor rsi, rsi ; clear rsi (second arg for syscall)
	syscall
	mov rcx, 1000
	mov rax, [rdi + 8] ; load microseconds portion into rax
	xor rdx, rdx ; clear edx for division
	div rcx ; divide by 1000 to get milliseconds
	mov rdx, [rdi] ; load seconds portion of time into rdx
	imul rdx, rcx ; multiply seconds by 1000 to get ms
	add rax, rdx ; add the ms from the microseconds and seconds together to get the current ms

	; milliseconds are in rax now

	mov [seed], rax ; Store the seed in the 'seed' variable

	ret


rand:
	; Algorithm to generate a pseudo-random number. returns in rax
	mov rax, [seed]
	imul rax, 1103515245
	add rax, 12345
	mov rbx, rax
	mov [seed], rbx

	; Return the generated number in rax
	mov rax, rbx
	and rax, 0x7FFFFFFF ; Ensure its a positive number
	ret


randint:
	; generate random integer in inclusive range
	; (rand() % (max - min + 1)) + min
	; rax - min
	; rbx - max
	; stores result in rax

	; preserve parameters
	push rax
	push rbx
	
	call rand ; store random int in rax

	; transfer random int into rcx and restore rax
	mov rcx, rax
	
	; restore saved parameters
	pop rbx
	pop rax

	; calculator divisor of modulo (rbx - rax + 1)
	sub rbx, rax
	inc rbx

	; divisor is now stored in rbx.

	; preserve rax (min value)
	push rax

	; do modulo operation
	xor rdx, rdx
	mov rax, rcx ; transfer random number (dividend) to rax
	div rbx ; rbx contains our divisor

	; rdx now contains the modulo result
	; restore rax
	pop rax

	; add rax and rdx to get our result
	; we will store in rax to return
	add rax, rdx

	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Integer printing system ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

calculate_ten_power:
	; calculate the power of 10 that corresponds to an integer
	; for example, 100 for 543, 1000 for 8956, and 10000 for 15236
	; takes an argument on the stack
	; returns the integer in rcx

	; rsp is the return address, add 8 to get the argument
	mov rcx, [rsp+8] ; rcx should be the integer to find the power of 10 for

	mov rax, 1 ; we need to calculate the power of 10 that corresponds to rcx
	; for example 100 for 543 and 1000 for 8753
	mov rbx, 10
	calculate_ten:
		mul rbx
		cmp rax, rcx
		jg finish_power_ten ; if number is greater than target, divide by 10 and ret
		jmp calculate_ten

	finish_power_ten:
		; divide ax by 10 to finish the calculation
		xor rdx, rdx
		div rbx
		; now rax contains the power of 10
		mov rcx, rax
	ret


print_digit:
	; print a digit
	; takes an argument on the stack
	; returns nothing
	push rcx
	mov rcx, [rsp+16] ; get the third argument on the stack. [return address (+0)] -> [rcx (+8)] -> [digit to print (+16)]
	add rcx, '0' ; convert digit to ASCII

	mov byte [str_buffer], cl ; assign lower 8 bits of rcx to buffer

	mov rsi, str_buffer ; buffer pointer
	mov rax, 1 ; write
	mov rdi, 1 ; stdout
	mov rdx, 1 ; len
	syscall   ; call kernel

	pop rcx ; restore rcx

	ret


print_integer:
	; takes the integer in from rax
	push rax ; push rax it for the next function to consume
	call calculate_ten_power ; power of 10 is now in rcx

	pop rax ; mov the argument (number to print) that was pushed into rax

	iter_number:
		; num_to_print: rax
		; base_10_place: rcx
		; formula for accessing number: (num_to_print // base_10_place) % 10
		; base_10_place is the power of 10 that corresponds to the place of number to print
		; using 123 for example, 100 will get the 1, 10 will get the 2, and 1 will get the 3

		; first, make sure we have a copy of rax
		push rax

		; 10 for use in modulo
		mov rbx, 10

		; next, floor divide rax by rcx
		xor rdx, rdx
		div rcx
		; result is stored in rax, mod 10
		; clear out rdx because thats where remainder is stored
		xor rdx, rdx
		div rbx

		; rdx now contains our digit to print
		push rdx
		call print_digit
		add rsp, 8 ; remove the rdx that never got popped from print_digit from the stack


		; check if rcx is equal to 1. if so, we just did the last digit
		mov rax, 1
		cmp rax, rcx
		je exit_print_integer

		; divide out power of 10 by 10 to get the next digit
		xor rdx, rdx
		mov rbx, 10
		mov rax, rcx

		div rbx
		mov rcx, rax

		; restore our original number to print
		pop rax

		; loop to iter_number until rcx is 1 (we've done the last digit)
		jmp iter_number

	exit_print_integer:
	pop rax ; pop off our original number so that we return to the correct address

	ret


;;;;;;;;;;;;;;;;;;;;
;; Game Functions ;;
;;;;;;;;;;;;;;;;;;;;

fail:
	mov rbx, 0

	outer_fail:
		inc rbx
		; print Failure
		mov rax, 1
		mov rdi, 1
		mov rsi, failure_msg
		mov rdx, failure_msg_len
		syscall

		mov rcx, rbx ; counter for inner loop
		inner_fail:
			; print !
			push rcx
			push '!'
			mov rax, 1
			mov rdi, 1
			mov rsi, rsp
			mov rdx, 1
			syscall

			; remove ! from stack
			add rsp, 8
			pop rcx

			dec rcx
			jnz inner_fail

			; print newline
			push 0xa
			mov rax, 1
			mov rdi, 1
			mov rsi, rsp
			mov rdx, 1
			syscall

			; restore stack
			add rsp, 8

			jmp outer_fail


seek_command:
	mov ax, [bamboo] ; move bamboo into ax
	cmp ax, 0
	jg seek_decrement_bamboo
	jmp seek_warn_no_bamboo

	seek_decrement_bamboo:
		; use up 1 bamboo
		dec ax
		mov [bamboo], ax
		jmp seek_after_bamboo_use

	seek_warn_no_bamboo:
		; user has no bamboo, warn them
		mov rsi, seek_no_bamboo_msg
		mov rdx, seek_no_bamboo_msg_len
		mov rax, 1
		mov rdi, 1
		syscall

		; we dont need to jump to seek_after_bamboo_use

	seek_after_bamboo_use: ; after bamboo is decremented

	mov rax, 0 ; rand min
	mov rbx, 100 ; rand max
	call randint ; store seeked value in rax
	
	cmp rax, 0
	je seek_bamboon ; you soke a bamboon

	cmp rax, 10
	jl seek_baboon ; you soke a baboon

	cmp rax, 50
	jl seek_bamboo ; you soke a bamboo

	jmp seek_nothing ; if you seek nothing

	seek_bamboon:
		; move message to print into registers
		mov rsi, seek_bamboon_message
		mov rdx, seek_bamboon_message_len
		mov rax, 1
		mov rdi, 1
		syscall

		jmp stumble_command

	seek_baboon:
		mov ax, [bamboo] ; check if bamboo is greater than 0
		cmp ax, 0
		jg seek_baboon_success
		jmp seek_baboon_fail

		seek_baboon_success:
			; increment baboons by 1
			mov ax, [baboons]
			inc ax
			mov [baboons], ax

			mov rsi, seek_baboon_message
			mov rdx, seek_baboon_message_len
			jmp seek_print_and_return

		seek_baboon_fail:
			; print message that the baboon ran away
			mov rsi, seek_baboon_fail_msg
			mov rdx, seek_baboon_fail_msg_len
			jmp seek_print_and_return

	seek_bamboo:
		; increment baboons by 1
		mov ax, [bamboo]
		add ax, 2
		mov [bamboo], ax

		mov rsi, seek_bamboo_message
		mov rdx, seek_bamboo_message_len
		jmp seek_print_and_return

	seek_nothing:
		mov rsi, seek_nothing_message
		mov rdx, seek_nothing_message_len
		jmp seek_print_and_return

	seek_print_and_return:
		; print the message that was moved into rsi and rdx 
		mov rax, 1
		mov rdi, 1
		syscall

		jmp user_input_loop


what_does_i_has_command:
	; print intro message
	mov rax, 1
	mov rdi, 1
	mov rsi, bamboo_first_half_msg
	mov rdx, bamboo_first_half_len
	syscall

	xor rax, rax
	mov ax, [bamboo]
	call print_integer

	; print second half of intro message
	mov rax, 1
	mov rdi, 1
	mov rsi, bamboo_second_half_msg
	mov rdx, bamboo_second_half_len
	syscall

	; " AND YOU HAS A Baboon Seeking Backpack "
	mov rax, 1
	mov rdi, 1
	mov rsi, inventory_backpack_msg
	mov rdx, inventory_backpack_len
	syscall

	; get count of baboons
	xor rcx, rcx
	mov cx, [baboons]

	test cx, cx
	je after_printing_inventory_loop

	printing_inventory_loop:
		push cx
		; " AND YOU HAS A Baboon " for every baboon that the user has
		mov rax, 1
		mov rdi, 1
		mov rsi, inventory_baboon_msg
		mov rdx, inventory_baboon_len
		syscall

		xor rcx, rcx
		pop cx

		loop printing_inventory_loop

	after_printing_inventory_loop:

	; print newline
	push 0xa
	mov rax, 1
	mov rdi, 1
	mov rsi, rsp
	mov rdx, 1
	syscall

	add rsp, 8 ; remove the newline from the stack

	jmp user_input_loop


stumble_command:
	mov rax, 1 ; rand min
	mov rbx, 2 ; rand max
	call randint ; store seeked value in rax

	xor rbx, rbx
	mov bx, [bamboo] ; load bamboo into bx
	test bx, bx ; test is bx (bamboo) is zero
	je stumble_drop_baboon

	cmp rax, 1 ; test is rax (random number) is zero
	je stumble_drop_baboon

	; 50/50 chance to select bamboo or baboon
	jmp stumble_drop_bamboo

	stumble_drop_bamboo:
		push rbx
		mov rax, 1 ; rand min
		; rbx is already set to the number of bamboo from the earlier comparison
		call randint ; store seeked value in rax

		pop rbx
		sub bx, ax ; subtract the number of bamboo lost
		push ax ; push the number of bamboo dropped to stack

		mov [bamboo], bx
		mov rax, bamboo

		; print "You stumbled and dropped "
		mov rax, 1
		mov rdi, 1
		mov rsi, stumble_bamboo_first_msg
		mov rdx, stumble_bamboo_first_len
		syscall

		pop ax ; recall the number of bamboo dropped
		call print_integer

		; print " bamboo\n"
		mov rax, 1
		mov rdi, 1
		mov rsi, stumble_bamboo_second_msg
		mov rdx, stumble_bamboo_second_len
		syscall

		jmp user_input_loop

	stumble_drop_baboon:
		mov rax, 0
		mov bx, [baboons] ; recall baboon count as rand max
		push bx

		call randint ; rand number in rax

		test rax, rax ; test is random number is 0 (you dropped baboon seeking backpack)
		je stumble_drop_backpack

		; you just dropped a baboon
		pop bx
		dec bx
		mov [baboons], bx

		; print "You stumbled and dropped a Baboon"
		mov rax, 1
		mov rdi, 1
		mov rsi, stumble_item_baboon
		mov rdx, stumble_item_baboon_len
		syscall

		jmp user_input_loop

		stumble_drop_backpack:
			; print "You stumbled and dropped a Baboon"
			mov rax, 1
			mov rdi, 1
			mov rsi, stumble_item_backpack
			mov rdx, stumble_item_backpack_len
			syscall

			jmp fail


_start:
	; seed rand
	call srand

	; print welcome message
	mov rax, 1
	mov rdi, 1
	mov rsi, welcome_msg
	mov rdx, welcome_msg_len
	syscall

	; print first part of instructions
	mov rax, 1
	mov rdi, 1
	mov rsi, instructions_msg_part1
	mov rdx, instructions_msg_part1_len
	syscall

	; generate random integer in range 5..=20
	mov rax, 5
	mov rbx, 20
	call randint

	; store integer result in bamboo
	mov [bamboo], rax

	; print random number
	call print_integer
	
	; print second part of instructions
	mov rax, 1
	mov rdi, 1
	mov rsi, instructions_msg_part2
	mov rdx, instructions_msg_part2_len
	syscall

	user_input_loop:
		call take_input

		; check if the user inputted "seek"
		mov rsi, user_input_buffer
		mov rdi, seek_string
		call compare_strings
		cmp rax, 0
		je seek_command

		; check if the user inputted "WHAT DOES I HAS"
		mov rsi, user_input_buffer
		mov rdi, inventory_string
		call compare_strings
		cmp rax, 0
		je what_does_i_has_command

		; user inputter invalid text; stumble
		jmp stumble_command

	; exit
exit_label:
	mov rax, 60
	mov rdi, 0
	syscall ; call kernel
